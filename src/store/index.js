import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import _ from 'lodash';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    coords: null,
    currentCoord: null,
    loading: false,
    currentTime: null,
    timeIndex: null,    
    prevCoord: null,
    nextCoord: null,
    timeDiff: null,
  },
  mutations: {
    SET_COORDS: (state, val) => state.coords = _.map(val, i => ([
      (new Date(i[0])).getTime(),
      i[2],
      i[1],
    ])),
    SET_LOADING: (state, val) => state.loading = val,
    SET_CURRENT_TIME: (state, val) => {
      state.currentTime = val;
      const timeIndex = Math.max(0, _.findIndex(state.coords, i => i[0] >= val) - 1);
      const prevTime = state.coords[timeIndex][0];
      const hasNextPoint = timeIndex + 1 < state.coords.length;
      const nextTime = hasNextPoint ? state.coords[timeIndex + 1][0] : prevTime;
      const timeProgress = hasNextPoint ? (val - prevTime) / (nextTime - prevTime) : 0;
      const prevCoord = [state.coords[timeIndex][1], state.coords[timeIndex][2]];
      const nextCoord = hasNextPoint ? [state.coords[timeIndex + 1][1], state.coords[timeIndex + 1][2]] : prevCoord;
      const vector = [
        nextCoord[0] - prevCoord[0],
        nextCoord[1] - prevCoord[1],
      ];
      state.currentCoord = [
        prevCoord[0] + vector[0] * timeProgress,
        prevCoord[1] + vector[1] * timeProgress,
      ];
      state.prevCoord = prevCoord;
      state.nextCoord = nextCoord;
      state.timeDiff = nextTime - prevTime;
      state.timeIndex = timeIndex;
    },
  },
  actions: {
    fetchData({ commit }) {
      commit('SET_LOADING', true);
      return axios.get('/coordinates.json')
      .then(res => {
        commit('SET_COORDS', res.data);
        commit('SET_LOADING', false);
      })
      .catch(() => {
        commit('SET_LOADING', false);
      })
    }
  },
  getters: {
    getLoading: state => state.loading,
    getMinTime: state => state.coords && state.coords[0][0],
    getMaxTime: state => state.coords && state.coords[state.coords.length - 1][0],
    getCurrentCoord: state => state.currentCoord || (state.coords && ([state.coords[0][1], state.coords[0][2]])),
    getCurrentSpeed: state => state.speed,
    getCurrentTime: (state, getters) => state.currentTime || getters.getMinTime,
    getCoordsLine: state => _.map(state.coords, i => ([i[1], i[2]])),
    getPrevCoord: state => state.prevCoord,
    getNextCoord: state => state.nextCoord,
    getTimeDiff: state => state.timeDiff,
    getTimeIndex: state => state.timeIndex,
  },
  modules: {
  }
})
